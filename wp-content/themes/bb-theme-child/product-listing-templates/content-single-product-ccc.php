<style>
.product-banner-img-ccc{
	text-align:center;
	margin-bottom:0;
}
.product-banner-img-ccc *{ 
	color:#fff;
}
.product-banner-img-ccc hr{
	width: 10%; border-color: #fff; margin-top: 5px;max-width:150px;
}

#productModal.fade{
	display:block;
}
#productModal .modal-header .modal_cls_btn{float:right;}
#productModal .modal-footer{text-align:right;}
#productModal{
	display:none;
    position: fixed;
    margin: auto;
    left: 0;
    right: 0;
    border: 0;
    background: #0000001f;
    top: 0;
    z-index: 999;
	bottom:0;
}
#productModal .modal-content {
    width: 700px;
    background: #fff;
    margin: 50px auto 0;
    padding: 20px;
}
#productModal .gform_wrapper .top_label input.medium,#productModal .gform_wrapper .top_label select.medium{
    width:100% !important;
}

</style>
<?php
global $post;
$flooringtype = $post->post_type; 

if(get_option('salesbrand')!=''){
	$brandonsale = array_filter(explode(",",get_option('salesbrand')));
}

 
$meta_values = get_post_meta( get_the_ID() );
$brand = $meta_values['brand'][0] ;
$sku = $meta_values['sku'][0];
$manufacturer = $meta_values['manufacturer'][0];
$room_image =  single_gallery_image_product(get_the_ID(),'1000','1600');
$room_image_mobile =  single_gallery_image_product(get_the_ID(),'400','400');

$image_600 = swatch_image_product(get_the_ID(),'600','400');

$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');

if(get_option('salesbrand')!=''){
	$slide_brands = rtrim(get_option('salesbrand'), ",");
	$brandonsale = array_filter(explode(",",$slide_brands));
	$brandonsale = array_map('trim', $brandonsale);
}
$image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 
	
	if(strpos($image , 's7.shawimg.com') !== false){
		if(strpos($image , 'http') === false){ 
		$image = "http://" . $image;
	}	
		$image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
	}else{
		if(strpos($image , 'http') === false){ 
		$image = "https://" . $image;
	}	
		$image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
	}	
?>

<?php
		// check if the repeater field has rows of data
        if(get_field('gallery_room_images')){			// loop through the rows of data
        
        $gallery_image = get_field('gallery_room_images');

        $gallery_i = explode("|",$gallery_image);

        foreach($gallery_i as  $key=>$value) {

			$room_image = $value;
			if(strpos($room_image , 's7.shawimg.com') !== false){
				if(strpos($room_image , 'http') === false){ 
					$room_image = "http://" . $room_image;
				}
				$room_image = $room_image ;
			}else{
				if(strpos($room_image , 'http') === false){ 
					$room_image = "https://" . $room_image;
				}
				$room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[1600x1000]&sink";
			}										
	?>
	<?php
			break;
        }
	}
	
	if(get_field('gallery_room_images')){			// loop through the rows of data
        
		$room_image =  single_gallery_image_product($post->ID,'1000','1600');
	}
	?>
	</div></div></div>
	<div class="product-banner-img product-banner-img-ccc" <?php if($room_image) {	?>style="background-image:url('<?php echo $room_image; ?>');"<?php } ?>>
		<?php if(get_field('collection')) { ?>
				<h3 class="fl-post-title" itemprop="name"><?php the_field('collection'); ?></h3>
				 <hr>
				<?php } ?>

				<h1 class="fl-post-title" itemprop="name"><?php the_title(); ?></h1>

				<?php if(get_field('brand_facet')) { ?>
				<i><?php the_field('brand_facet'); ?></i>
		<?php } ?>
	</div>
	<?php echo do_shortcode('[fl_builder_insert_layout slug="coupon-sale-row"]'); ?>
	<div class="container">
		<div class="row">
			<div class="fl-content product col-sm-12 ">
				<div class="product-detail-layout-ccc">
					<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
						<div class="fl-post-content clearfix grey-back" itemprop="text">
							<div class="row">
								<div class="col-md-5 col-md-offset-1 col-sm-12 col-xs-12 product-swatch">   
									<?php 
										$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images.php';
										include( $dir );
									?>
								</div>
								<div class="col-md-5 col-sm-12 col-xs-12 product-box">
									<?php
										// $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
										// include_once( $dir );
									?>
									<div class="clearfix"></div>
									<?php if(get_field('collection')) { ?>
									<h3 class="fl-post-title" itemprop="name"><?php the_field('collection'); ?></h3>
									<?php } ?>

									<h2 class="fl-post-title" itemprop="name"><?php the_title(); ?></h2>

									<?php if(get_field('brand_facet')) { ?>
									<i><?php the_field('brand_facet'); ?></i>
									<?php } ?>
									<?php if(get_field('in_stock')== 1) { ?>
									<h2 class="in_stock_lable"><i>In-Stock</i></h2>
									<?php } ?>

									
									<div class="product-colors">
										<?php
											$collection = $meta_values['collection'][0];    
											$satur = array('Masland','Dixie Home');  
											
										if($collection != NULL ){
											
											if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

												$familycolor = $meta_values['style'][0];
												$key = 'style';

											}else{

												$familycolor = $meta_values['collection'][0];    
												$key = 'collection';
												
											}	
										}else{	
											
											if (in_array($meta_values['brand'][0], $satur)){
												$familycolor = $meta_values['design'][0];    
												$key = 'design';
											  }

										}	

											$args = array(
												'post_type'      => $flooringtype,
												'posts_per_page' => -1,
												'post_status'    => 'publish',
												'meta_query'     => array(
													array(
														'key'     => $key,
														'value'   => $familycolor,
														'compare' => '='
													),
													array(
														'key' => 'swatch_image_link',
														'value' => '',
														'compare' => '!='
														)
												)
											);										
									
											$the_query = new WP_Query( $args );
										?>
										<!-- <ul>
											<li class="color-count" style="font-size:14px;"><?php //echo $the_query->found_posts; ?> Colors Available</li>
										</ul> -->
									</div>
									<div class="button-wrapper">
										<?php if( get_option('getcouponbtn') == 1 ){  ?>
																<a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/coupon/'; } ?>" target="_self" class="button alt getcoupon-btn yellow" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
																<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?>
																</a>
										<?php } ?>
										<a href="/shop-at-home/"  class="button contact-btn orange">Shop at Home</a>
										
										<?php if(get_option('pdp_get_finance') != 1 || get_option('pdp_get_finance') == '' ){?>						
											<a href="/services/request-financing/" class="finance-btn button orange">Request Financing</a>	
										<?php } ?> 

										<?php  roomvo_script_integration($manufacturer,$sku,get_the_ID()); ?>
										

										<a  href="#" class="link  enquiry_link"  data-toggle="modal"  id="productModalLink" data-product="<?php the_title(); ?>" 
										data-title="<?php the_title(); ?>" data-brand="<?php the_field('brand_facet'); ?>"  data-product_number="<?php the_field('sku'); ?>" 
										data-product_family="<?php the_field('collection'); ?>">Product Inquiry</a>

										<a href="/schedule-an-appointment/"  class="link appointment_link">Request Appointment</a>
									</div>
								</div>
									<div class="clearfix"></div>
							</div>
								<div class="clearfix"></div>
							<?php
								$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider.php';
								include( $dir );
							?>
							
						</div>
						
					</article>
				</div>
			</div>	
		</div>
	</div>	

	<?php
		$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/modal-product-ccc.php';
		include( $dir );
	?>	
	<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>