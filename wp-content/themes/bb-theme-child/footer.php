<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer class="fl-page-footer-wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WPFooter"' ); ?>  role="contentinfo">
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();

?>

<script>
	$(function () {
		$('.calculateBtn').m2Calculator({
			measureSystem: "Imperial",            
			thirdPartyName: "CCC", 
			thirdPartyEmail: "jmcdonnell@customcarpetcenters.com",  
			showCutSheet: false, 
			showDiagram: true, 
			cancel: function () {
			
			},
			callback: function (data) {
				
				var json = JSON.parse(JSON.stringify(data));

				var res = JSON.stringify(data);
				//  $("#callback").html(JSON.stringify(data));
				var json1 = JSON.parse(JSON.stringify(json.input));                      
				var imageName = '';        
				if (!data.input.rooms.length){
					imageName = data.input.stairs[0].name;
				}else{
					imageName = data.input.rooms[0].name;
				}
				window.location.href = "/measurement-tool/thank-you/";
			}
		});
	
	});        
</script>
</body>
</html>
