  // header flyer choose location js start here 
  jQuery(document).on('click', '.choose_location', function() {
  
    var mystore = jQuery(this).attr("data-id");
    var distance = jQuery(this).attr("data-distance");
    var storename = jQuery(this).attr("data-storename");

    jQuery.cookie("preferred_store", null, { path: '/' });
    jQuery.cookie("preferred_distance", null, { path: '/' });
    jQuery.cookie("preferred_storename", null, { path: '/' });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location&store_id=' + jQuery(this).attr("data-id") + '&distance=' + jQuery(this).attr("data-distance"),

        success: function(data) {

            jQuery(".header_store").html(data);
            jQuery.cookie("preferred_store", mystore, { expires: 1, path: '/' });
            jQuery.cookie("preferred_distance", distance, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storename", storename, { expires: 1, path: '/' });
            setTimeout(addFlyerEvent, 1000);
            closeNav();
            iconsClick();
            jQuery(".contentFlyer").css('display', 'block');

            var loc = storename+', NY';
			if(storename==undefined || storename=="undefined"){} else {
            	jQuery("#input_81_32").val(loc);
			}
            
        }
    });

});

// flyer close function 
function closeNav() {
    let storeLocation = document.getElementById("storeLocation");
    storeLocation.style.left = "-100%";
    storeLocation.classList.remove("ForOverlay");
}

// flyer event assign function
function addFlyerEvent() {
    var flyerOpener = document.getElementById('openFlyer');
    if(flyerOpener){
        flyerOpener.addEventListener("click", function(e) {
            let storeLocation = document.getElementById("storeLocation");
            storeLocation.style.left = "0";
            storeLocation.classList.add("ForOverlay");
        });
    }
   
}
function iconsClick(value){
    jQuery(document).ready(function(){
        jQuery(".locationWrapFlyer .icons i").click(function(){
            jQuery(".contentFlyer").animate({
                width: "toggle"
            });
        });
    });
}
iconsClick();
jQuery(document).ready(function() {

    var storename =  jQuery.cookie("preferred_storename");
    var loc = storename+', NY';
	if(storename==undefined || storename=="undefined"){} else{
    	jQuery("#input_81_32").val(loc).change();
	}

    jQuery(function($) {
        $('#storeLocation .choose_location').on('click', closeNav);

    });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=get_storelisting',
        dataType: 'JSON',
        success: function(response) {
           
                var posts = JSON.parse(JSON.stringify(response));
                var header_data = posts.header;
                var list_data = posts.list;

                var mystore_loc =  jQuery.cookie("preferred_storename");
                var loc = mystore_loc+', NY';
				if(mystore_loc==undefined || mystore_loc=="undefined"){} else {
                jQuery("#input_81_32").val(loc).change();
				}
                jQuery(".header_store").html(header_data);
                setTimeout(addFlyerEvent, 1000);
                jQuery("#ajaxstorelisting").html(list_data);
                iconsClick();      
        }
    });

});

jQuery(document).ready(function() {

    var mystore_loc =  jQuery.cookie("preferred_storename");
    //console.log(mystore_loc);
    mystore_locadded = mystore_loc+', NY';
    jQuery(".populate-store select").val(mystore_loc);
	if(mystore_loc==undefined || mystore_loc=="undefined"){} else {
		jQuery("#input_81_32").val(mystore_locadded).change();	
	}

    jQuery(".facet_filters .fl-html").prepend('<div class="close_bar"><a class="close close_sidebar_1" href="#"><i class="fa fa-close">&nbsp;</i></a></div>')

    jQuery(".close_bar .close_sidebar_1").click(function(e){
       //alert('hi');
       e.preventDefault();
       jQuery(".facet_filters").animate({"left":-jQuery(".facet_filters").outerWidth()-20},500);
    });
    jQuery(".facet_filters .close_sidebar").click(function(e){
        //alert('hi');
        e.preventDefault();
        jQuery(".facet_filters").animate({"left":-jQuery(".facet_filters").outerWidth()-20},500);
    });

    jQuery(".open_sidebar").click(function(e){
        e.preventDefault();
        jQuery(".facet_filters").css("left",-jQuery(".facet_filters").outerWidth()-20);
        jQuery(".facet_filters").animate({"left":0},500);
    });
});

