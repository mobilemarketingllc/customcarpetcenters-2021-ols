<?php
@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');
// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri() . "/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri() . "/resources/jquery.cookie.js", "", "", 1);
    wp_enqueue_script("child-script", get_stylesheet_directory_uri() . "/script.js", "", "", 1);
    wp_enqueue_script("m2FlooringCalculator", "https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js", "", "");
});

// Disable Updraft and Wordfence on localhost
add_action('init', function () {
    if (!is_admin() && (strpos(get_site_url(), 'localhost') > -1)) {
        include_once(ABSPATH . 'wp-admin/includes/plugin.php');

        $updraft = WP_PLUGIN_DIR . '/updraftplus/updraftplus.php';
        if (file_exists($updraft)) deactivate_plugins([$updraft]);

        $wordfence = WP_PLUGIN_DIR . '/wordfence/wordfence.php';
        if (file_exists($wordfence)) deactivate_plugins([$wordfence]);
    }
});

// Register menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'footer-1' => __('Footer Menu 1'),
            'footer-2' => __('Footer Menu 2'),
            'footer-3' => __('Footer Menu 3'),
            'footer-4' => __('Footer Menu 4'),
            'footer-5' => __('Footer Menu 5'),
            'site-map' => __('Site Map'),
        )
    );
}
add_action('init', 'register_my_menus');



// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');


// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');



//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output = '<div class="facet-wrap"><strong>' . $atts['title'] . '</strong>' . $output . '</div>';
    }
    return $output;
}, 10, 2);

add_filter('facetwp_result_count', function ($output, $params) {
    $output = $params['total'] . ' Results';
    return $output;
}, 10, 2);

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter('wpseo_breadcrumb_links', 'custom_yoast_breadcrumb_trail');

function custom_yoast_breadcrumb_trail($links)
{
    global $post;

    if (is_singular('hardwood_catalog')) {
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/hardwood-flooring/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/product-category/hardwood/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('carpeting')) {
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/carpeting/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/product-category/carpeting/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('luxury_vinyl_tile')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/luxury-vinyl-tile/',
            'text' => 'Luxury Vinyl Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/product-category/luxury-vinyl-tile/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('laminate_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/laminate-flooring/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/product-category/laminate/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('tile_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/ceramic-tile/',
            'text' => 'Ceramic Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/product-category/ceramic-tile/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('solid_wpc_waterproof')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/product-category/waterproof-flooring/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('area_rugs')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/area-rugs/',
            'text' => 'Area Rugs',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/product-category/area-rugs/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    return $links;
}


//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter('wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail_custom', 90);

function wpse_override_yoast_breadcrumb_trail_custom($links)
{

    if (is_page('hardwood')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Product Category',
        );

        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_page('laminate')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Product Category',
        );

        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_page('luxury-vinyl-tile')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Product Category',
        );

        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_page('waterproof-flooring')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Product Category',
        );

        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_page('ceramic-tile')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Product Category',
        );

        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_page('stainmaster')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Product Category',
        );

        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_page('carpeting')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Product Category',
        );

        array_splice($links, 1, -1, $breadcrumb);
    }

    return $links;
}

wp_clear_scheduled_hook('action_scheduler_run_queue');

function asdds_disable_default_runner()
{
    if (class_exists('ActionScheduler')) {
        remove_action('action_scheduler_run_queue', array(ActionScheduler::runner(), 'run'));
    }
}
// ActionScheduler_QueueRunner::init() is attached to 'init' with priority 1, so we need to run after that
add_action('init', 'asdds_disable_default_runner', 10);


/*
  //IP location FUnctionality
  if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
    wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
}

add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' ); */

function cde_preferred_location()
{

    global $wpdb;

    if (! function_exists('post_exists')) {
        require_once(ABSPATH . 'wp-admin/includes/post.php');
    }

    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);


    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {

        //API Call for getting website INFO
        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $website = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $inputs, $headers);


        // write_log($website['result']['locations']);

        for ($i = 0; $i < count($website['result']['locations']); $i++) {

            if ($website['result']['locations'][$i]['type'] == 'store') {

                $location_name = isset($website['result']['locations'][$i]['city']) ? $website['result']['locations'][$i]['city'] : "";

                $found_post = post_exists($location_name, '', '', 'store-locations');

                if ($found_post == 0) {

                    $array = array(
                        'post_title' => $location_name,
                        'post_type' => 'store-locations',
                        'post_content'  => "",
                        'post_status'   => 'publish',
                        'post_author'   => 0,
                    );
                    $post_id = wp_insert_post($array);

                    update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']);
                    update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']);
                    update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']);
                    update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']);
                    update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']);
                    if ($website['result']['locations'][$i]['forwardingPhone'] == '') {

                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);
                    } else {

                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);
                    }

                    update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']);
                    update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']);
                    update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']);
                    update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']);
                    update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']);
                    update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']);
                    update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']);
                    update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']);
                    update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']);
                } else {

                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']);
                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']);
                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']);
                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']);
                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']);
                    if ($website['result']['locations'][$i]['forwardingPhone'] == '') {

                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);
                    } else {

                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);
                    }

                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']);
                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']);
                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']);
                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']);
                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']);
                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']);
                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']);
                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']);
                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']);
                }
            }
        }
    }
}



//get ipaddress of visitor

function getUserIpAddr()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    if (strstr($ipaddress, ',')) {
        $tmp = explode(',', $ipaddress, 2);
        $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long

function get_storelisting()
{

    global $wpdb;
    $content = "";


    //  echo 'no cookie';

    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post(
        $urllog,
        array(
            'method' => 'GET',
            'timeout' => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'headers' => array('Content-Type' => 'application/json'),
            'body' =>  array('u' => 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0', 'ip' => getUserIpAddr(), 'dbs' => 'all', 'trans_id' => 'example', 'json' => 'true'),
            'blocking' => true,
            'cookies' => array()
        )
    );

    $rawdata = json_decode($response['body'], true);
    $userdata = $rawdata['response'];

    $autolat = $userdata['pulseplus-latitude'];
    $autolng = $userdata['pulseplus-longitude'];
    if (isset($_COOKIE['preferred_store'])) {

        $store_location = $_COOKIE['preferred_store'];
    } else {

        $store_location = '';
    }

    //print_r($rawdata);

    $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(" . $autolat . " ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( " . $autolng . " ) ) + sin( radians( " . $autolat . " ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

    // write_log($sql);

    $storeposts = $wpdb->get_results($sql);
    $storeposts_array = json_decode(json_encode($storeposts), true);


    if ($store_location == '') {
        //write_log('empty loc');           
        $store_location = $storeposts_array['0']['ID'];
        $store_distance = round($storeposts_array['0']['distance'], 1);
    } else {

        // write_log('noempty loc');
        $key = array_search($store_location, array_column($storeposts_array, 'ID'));
        $store_distance = round($storeposts_array[$key]['distance'], 1);
    }

    $content .= '<div class="header_store"><div class="store_wrapper">';
    $content .= '<div class="locationWrapFlyer">
      
      <div class="contentFlyer"><div> 
          <p>Your Nearest Showroom</p>';
    $content .= '<h3 class="title-prefix">' . get_the_title($store_location) . '</h3>';
    $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">View All Locations</a></h5>';
    $content .= '</div></div></div>';



    $content_list = '<div id="storeLocation" class="changeLocation">';
    $content_list .= '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>';
    $content_list .= '<div class="content">';

    foreach ($storeposts as $post) {

        // write_log(get_field('phone', $post->ID));

        $content_list .= '<div class="store_wrapper" id ="' . $post->ID . '">';
        $content_list .= '<h5 class="title-prefix">' . get_the_title($post->ID) . '  - <b>' . round($post->distance, 1) . ' MI</b></h5>';
        $content_list .= '<h5 class="store-add">' . get_field('address', $post->ID) . ' ' . get_field('city', $post->ID) . ', ' . get_field('state', $post->ID) . ' ' . get_field('postal_code', $post->ID) . '</h5>';

        if (get_field('closed', $post->ID)) {

            $content_list .= '<p class="store-hour redtext">' . get_field('hours', $post->ID) . '</p>';
            $content_list .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
        } else {

            $day = get_field(strtolower(date("l")), $post->ID);

            if ($day == 'Closed') {

                $content_list .= '<p class="store-hour">CLOSED TODAY</p>';
            } else {
                $daystr = strval($day);
                $closedtime = explode("-", $daystr);
                //  write_log($closedtime);               
                $content_list .= '<p class="store-hour">OPEN UNTIL ' . trim($closedtime[1]) . '</p>';
            }
        }
        $content_list .= '<p class="store-hour"><span class="phoneIcon"></span> &nbsp;<a href="tel:' . get_field('phone', $post->ID) . '">' . get_field('phone', $post->ID) . '</a></p>';
        $content_list .= '<a href="javascript:void(0)" data-id="' . $post->ID . '" data-storename="' . get_the_title($post->ID) . '" data-distance="' . round($post->distance, 1) . '" target="_self" class="store-cta-link choose_location">Choose This Location</a>';
        if (get_field('store_link', $post->ID)) {
            $content_list .= '<br><a href="' . get_field('store_link', $post->ID) . '" target="_self" class="store-cta-link view_location"> View Location</a>';
        }
        $content_list .= '</div>';
    }

    $content_list .= '</div>';
    $content_list .= '</div>';

    $data = array();

    $data['header'] = $content;
    $data['list'] = $content_list;

    //write_log($data['list']);

    echo json_encode($data);
    wp_die();
}

//add_shortcode('storelisting', 'get_storelisting');
add_action('wp_ajax_nopriv_get_storelisting', 'get_storelisting');
add_action('wp_ajax_get_storelisting', 'get_storelisting');



//choose this location FUnctionality

add_action('wp_ajax_nopriv_choose_location', 'choose_location');
add_action('wp_ajax_choose_location', 'choose_location');

function choose_location()
{

    $content = '<div class="store_wrapper">';
    $content .= '<div class="locationWrapFlyer">
     
      <div class="contentFlyer"><div>
          <p>Your Nearest Showroom</p>';

    $content .= '<h3 class="title-prefix">' . get_the_title($_POST['store_id']) . '</h3>';

    $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">View All Locations</a></h5>';
    $content .= '</div></div>';

    echo $content;

    wp_die();
}

//   function my_after_header() {
//       echo '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
//       <div class="locationOverlay"></div>';
//     }
//     add_action( 'fl_after_header', 'my_after_header' );

// function location_header() {
//     return '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
//     <div class="locationOverlay"></div>';
//   }
// add_shortcode('location_code', 'location_header');


add_filter('gform_pre_render_39', 'populate_product_location_form');
add_filter('gform_pre_validation_39', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_39', 'populate_product_location_form');
add_filter('gform_admin_pre_render_39', 'populate_product_location_form');

add_filter('gform_pre_render_38', 'populate_product_location_form');
add_filter('gform_pre_validation_38', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_38', 'populate_product_location_form');
add_filter('gform_admin_pre_render_38', 'populate_product_location_form');

add_filter('gform_pre_render_56', 'populate_product_location_form');
add_filter('gform_pre_validation_56', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_56', 'populate_product_location_form');
add_filter('gform_admin_pre_render_56', 'populate_product_location_form');

add_filter('gform_pre_render_37', 'populate_product_location_form');
add_filter('gform_pre_validation_37', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_37', 'populate_product_location_form');
add_filter('gform_admin_pre_render_37', 'populate_product_location_form');

add_filter('gform_pre_render_40', 'populate_product_location_form');
add_filter('gform_pre_validation_40', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_40', 'populate_product_location_form');
add_filter('gform_admin_pre_render_40', 'populate_product_location_form');


function populate_product_location_form($form)
{

    foreach ($form['fields'] as &$field) {

        // Only populate field ID 12
        if ($field->type != 'select' || strpos($field->cssClass, 'populate-store') === false) {
            continue;
        }

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );

        $locations =  get_posts($args);

        $choices = array();

        foreach ($locations as $location) {

            if (($form['id'] == 89) || ($form['id'] == 37) || ($form['id'] == 56) || ($form['id'] == 38)) {
                if (get_the_title($location->ID) == "Lakewood") {
                    continue;
                }
            }



            if (get_the_title($location->ID) == "Olean") {
                $title = get_the_title($location->ID) . ', NY - Shop at Home ONLY';
                $choices[] = array('text' => $title, 'value' => $title);
            } else {
                $title = get_the_title($location->ID) . ', NY';
                $choices[] = array('text' => $title, 'value' => $title);
            }
        }
        wp_reset_postdata();

        // write_log($choices);

        // Set placeholder text for dropdown
        $field->placeholder = '-- Choose Location --';

        // Set choices from array of ACF values
        $field->choices = $choices;
    }
    return $form;
}

//add method to register event to WordPress init

add_action('init', 'register_daily_mysql_bin_log_event');

function register_daily_mysql_bin_log_event()
{
    // make sure this event is not scheduled
    if (!wp_next_scheduled('mysql_bin_log_job')) {
        // schedule an event
        wp_schedule_event(time(), 'daily', 'mysql_bin_log_job');
    }
}

add_action('mysql_bin_log_job', 'mysql_bin_log_job_function');


function mysql_bin_log_job_function()
{

    global $wpdb;
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'";
    $delete_endpoint = $wpdb->get_results($sql_delete);
    // write_log($sql_delete);	
}

add_filter('auto_update_plugin', '__return_false');


function my_cache_lifetime($seconds)
{
    return 5184000; // 10 day. Default: 3600 (one hour)
}
add_filter('facetwp_cache_lifetime', 'my_cache_lifetime');


//theme loop template changes for instock PLP
// add_filter( 'facetwp_template_html', function( $output, $class ) {

//     $prod_list = $class->query;  
//     ob_start();       

//     if(isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1){

//         $dir = get_stylesheet_directory() . '/product-listing-templates/product-loop-new-theme.php';

//     }else{        

//         $dir = get_stylesheet_directory() . '/product-listing-templates/product-loop-new.php';

//     }

//     require_once $dir;    
//     return ob_get_clean();
// }, 10, 2 );


//sigle instock post type template

// add_filter('single_template', 'childtheme_get_custom_post_type_template');

function childtheme_get_custom_post_type_template($single_template)
{
    global $post;

    if ($post->post_type != 'post') {

        $single_template = get_stylesheet_directory() . '/product-listing-templates/single-' . $post->post_type . '.php';
    }
    return $single_template;
}


wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );
if (!wp_next_scheduled('ccc_404_redirection_301_log_cronjob')) {

    $interval =  getIntervalTime('friday');
    wp_schedule_event(time() +  17800, 'daily', 'ccc_404_redirection_301_log_cronjob');
}

add_action('ccc_404_redirection_301_log_cronjob', 'ccc_custom_404_redirect_hook');

function ccc_custom_404_redirect_hook()
{
    global $wpdb;
    // write_log('in function');

    $table_redirect = $wpdb->prefix . 'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix . 'redirection_groups';

    $data_404 = $wpdb->get_results("SELECT * FROM $table_name");
    $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;

    if ($data_404) {
        foreach ($data_404 as $row_404) {

            if (strpos($row_404->url, 'carpeting') !== false && strpos($row_404->url, 'product-category') !== false) {
                //  write_log($row_404->url);      

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {


                    $destination_url_carpet = '/product-category/carpeting/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_carpet,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');

                    //// write_log( 'carpet 301 added ');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }
            } else if (strpos($row_404->url, 'hardwood') !== false && strpos($row_404->url, 'product-category') !== false ) {

                // write_log($row_404->url);

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');
                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == '' && $headers_res == 'false') {


                    $destination_url_hardwood = '/product-category/hardwood/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_hardwood,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                // write_log( 'hardwood 301 added ');

            } else if (strpos($row_404->url, 'laminate') !== false && strpos($row_404->url, 'products-category') !== false ) {

                //   write_log($row_404->url); 

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == '' && $headers_res == 'false') {


                    $destination_url_laminate = '/product-category/laminate/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_laminate,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                //write_log( 'laminate 301 added ');


            } else if ((strpos($row_404->url, 'luxury-vinyl') !== false) && strpos($row_404->url, 'product-category') !== false ) {

                // write_log($row_404->url);  

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == '' && $headers_res == 'false') {


                     
                        $destination_url_lvt = '/product-category/luxury-vinyl-tile/';
                   

                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_lvt,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                // write_log( 'luxury-vinyl 301 added ');
            } else if (strpos($row_404->url, 'tile') !== false && strpos($row_404->url, 'product-category') !== false ) {

                // write_log($row_404->url); 

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == '' && $headers_res == 'false') {

                    $destination_url_tile = '/product-category/tile/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_tile,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                // write_log( 'tile 301 added ');
            }
        }
    }
}
